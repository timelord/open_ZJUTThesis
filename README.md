## 浙江工业大学计算机学院本科毕业设计模板(非官方)

本项目fork自[精弘网络团队](http://github.com/ZJUT)维护的repo[精弘网络版毕设模板](https://github.com/ZJUT/zjutthesis)

本项目目前维护者：BourneFang(tardis_fang#126.com)

精弘网络版毕设模板目前维护者:
[SuzieWong](http://www.imsuzie.com)

原项目fork于[Unlucky](http://blog.thebeyond.name)的童鞋的Repo，感谢他们的辛苦付出，附[原始模板地址](https://github.com/unlucky/zjutthesis),

原始作者: 
[Unlucky](http://blog.thebeyond.name)

原始贡献者: 
[McKelvin](https://github.com/mckelvin), 
[Brilliant](https://github.com/Brilliant)

## 0x00 简介

本模板是以浙江工业大学计算机学院本科生毕业设计论文要求
（[Word模板](http://www.software.zjut.edu.cn/index.php?m=news&a=view&left=gaikuang&id=2664)）设计的XeLaTeX模板，
经2013届多位同学试用没问题。学院表示可以接受PDF版本论文。

## 0x01 文件结构

 - report文件夹包含
  开题报告(proposal.tex)、
  文献综述(literaturereview.tex)、
  外文翻译(translation.tex)模板
 - thesis文件夹包含毕业设计论文(zjutmain.tex)模板

## 0x02 编译方法

### Linux

我的编译环境是Debian系统，软件是apt-get install texlive-full。Ubuntu系的应该也是一样的，Fedora系的暂不清楚。
另外需要解决Linux系统下使用MS的字体问题，可以自行下载所缺字体添加到系统。

文件夹中有的Makefile文件，使用相应的make命令即可编译tex文件得到相应的pdf文件，或者删除编译产生的不必要的文件。
**注意：如果添加了必要的新文件，请务必立即使用git add命令添加文件到git库，原因请查看Makefile文件。**


### Windows

具体编译的系统环境以及软件套件我暂不清楚，待测试。编译时，使用各个文件夹中对应的make.bat文件即可自动编译。
使用clean.bat可以清除冗余文件

## 0x03 注意事项

由于时间和能力有限，故该模板可能与学院要求的格式不尽相同。
作者不对使用该模板所造成的任何后果负责，但欢迎提供修改意见，以帮助我们完善该模板。

## 0x04 其他学院模板
[理学院](https://github.com/liuzheng712/zjutthesisLXY)，[机械学院](https://github.com/diufanshu/zjutthesis)
